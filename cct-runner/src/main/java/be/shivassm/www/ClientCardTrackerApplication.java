package be.shivassm.www;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientCardTrackerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClientCardTrackerApplication.class, args);


    }
}