package be.shivassm.www.rest.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.text.DateFormat;

@Document(collection = "clientCardMappings")
public class ClientCardMapping {

    @Id
    private String id;
    private String clientFullName;
    private Long giftCardActivatedOn;

    public ClientCardMapping(final String clientFullName) {
        this.clientFullName = clientFullName;
        this.giftCardActivatedOn = System.currentTimeMillis();
    }

    public ClientCardMapping() {
    }

    public String getId() {
        return id;
    }

    public String getClientFullName() {
        return clientFullName;
    }

    public String getActivationDate() {
        return DateFormat.getDateInstance(DateFormat.MEDIUM).format(giftCardActivatedOn);
    }
}
