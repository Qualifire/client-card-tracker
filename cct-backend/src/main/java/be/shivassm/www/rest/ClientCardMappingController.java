package be.shivassm.www.rest;

import be.shivassm.www.rest.db.ClientCardMappingRepo;
import be.shivassm.www.rest.exceptions.EmptyRequestBodyException;
import be.shivassm.www.rest.model.ClientCardMapping;
import be.shivassm.www.rest.db.ClientCardMappingRepo;
import be.shivassm.www.rest.exceptions.EmptyRequestBodyException;
import be.shivassm.www.rest.model.ClientCardMapping;
import be.shivassm.www.rest.form.ClientCardMappingForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/rest/clients")
public class ClientCardMappingController {

    @Autowired
    private ClientCardMappingRepo clientCardMappingRepo;

    @RequestMapping(value = "/cards", method = RequestMethod.GET)
    public List<ClientCardMapping> getAll() {
        return clientCardMappingRepo.findAll();
    }


    @RequestMapping(value = "/cards", params = "name", method = RequestMethod.GET)
    public List<ClientCardMapping> findForName(@RequestParam(value = "name") String userName) {
        return clientCardMappingRepo.findByName(userName.toLowerCase());
    }

    @RequestMapping(value = "/cards", method = RequestMethod.POST)
    public ResponseEntity<?> createMapping(@RequestBody @Valid ClientCardMappingForm clientCardMappingForm) {
        notNullRequestBody(clientCardMappingForm);

        clientCardMappingRepo.save(new ClientCardMapping(clientCardMappingForm.getFullName().toLowerCase()));

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/cards/{id}", name = "id", method = RequestMethod.DELETE)
    public void delete(@PathVariable String id) {
        clientCardMappingRepo.delete(id);

    }

    private void notNullRequestBody(Object body) {
        if (body == null) {
            throw new EmptyRequestBodyException();
        }
    }


}
