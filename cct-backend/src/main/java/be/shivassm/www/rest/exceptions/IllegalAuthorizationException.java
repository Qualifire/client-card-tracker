package be.shivassm.www.rest.exceptions;

/**
 * Created by juyttersprot on 9/12/2015.
 */
public class IllegalAuthorizationException extends IllegalArgumentException {

    public IllegalAuthorizationException(final String s) {
        super(s);
    }
}
