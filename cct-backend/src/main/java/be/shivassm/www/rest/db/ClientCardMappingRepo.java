package be.shivassm.www.rest.db;

import be.shivassm.www.rest.model.ClientCardMapping;
import be.shivassm.www.rest.model.ClientCardMapping;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientCardMappingRepo extends MongoRepository<ClientCardMapping, String> {

    @Query("{'clientFullName':{'$regex':?0}}")
    List<ClientCardMapping> findByName(String name);
}
