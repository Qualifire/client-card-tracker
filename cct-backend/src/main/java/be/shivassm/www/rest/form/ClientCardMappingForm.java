package be.shivassm.www.rest.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ClientCardMappingForm {

    @NotNull
    @Size(min = 1)
    private String fullName;

    ClientCardMappingForm() {
    }

    public ClientCardMappingForm(final String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }
}
